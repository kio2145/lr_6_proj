from sqlalchemy import create_engine, text

class Sql_connector():
    def __init__(self):
        self.engine = create_engine("mysql+mysqlconnector://sammy:passwd123@192.168.56.12:3306/students")
        self.out_elem=[]
    def start_connect(self):
        with self.engine.connect() as connection:
            result = connection.execute(text("SELECT * FROM Persons;"))
            for row in result:
                self.out_elem.append(row)
    def get_out_elem(self):
        self.start_connect()
        return self.out_elem
    def add_stud(self, stud_list):
         print("INSERT INTO Persons (PersonID, LastName, FirstName, \
                                              Address,City) VALUES ("+stud_list[0]+" , \""+stud_list[1]+"\",\""+stud_list[2]+"\","\
                                              "+\""+stud_list[3]+"\", \""+ stud_list[4]+"\");")
         with self.engine.connect() as connection:
            result = connection.execute(text("INSERT INTO Persons (PersonID, LastName, FirstName, \
                                              Address,City) VALUES ("+stud_list[0]+" , \""+stud_list[1]+"\",\""+stud_list[2]+"\","\
                                              "+\""+stud_list[3]+"\", \""+ stud_list[4]+"\");"))
            connection.commit()
             
